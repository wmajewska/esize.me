function initialize() {
    var layer = "terrain";
        var map = new L.Map(layer, {
            center: new L.LatLng(52.069339, 19.480368),
            zoom: 6
        });
        var jsonFeatures = [];

      address.forEach(function(point){
    var lat = point.lat;
    var lon = point.lng;
    var place= point.place;

    var feature = {type: 'Feature',
        properties: point,
        geometry: {
            type: 'Point',
            coordinates: [lon,lat]
        }
    };
    jsonFeatures.push(feature);
        var geoJson = { type: 'FeatureCollection', features: jsonFeatures };
        L.geoJson(geoJson);
        map.addLayer(new L.StamenTileLayer(layer, {
            detectRetina: true
         }));
         addPopup(point, map);
        });
}
function addPopup(address, map){
  var contentString = '<div id="content">'+
             '<h1 id="firstHeading" class="firstHeading">'+address.place+'</h1>'+
             '<div id="bodyContent">'+
             '<p><b>Godziny otwarcia:<br></b>'+
                  address.working_hours +
             '<p><b>Adres:<br></b>'+
                  address.address +
             '<p><b>Kontakt:<br></b>'+
            '<img src="imgs/phone.png" alt="">&nbsp'+address.contact+
                  '<br>'+
            '<img src="imgs/mail.png" alt="">&nbsp'+address.mail +
             '</div>'+
             '</div>';
  L.marker([address.lat, address.lng]).addTo(map).bindPopup(contentString);
}
$(function(){
$("#footer-content-container").load("footer.html");
$("#header-nav").load("header.html");
});
function openNav() {
document.getElementById("navigation").style.width = "100%";
}

function closeNav() {
document.getElementById("navigation").style.width = "0%";
}
