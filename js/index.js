
$(function(){
  $("#footer-content-container").load("footer.html");
});

if ($(window).width() < 800){
  $('#logo-change').attr("src","imgs/logo_color.png");
  $("div.tablet-img").html('<a id="yt-frame" target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=uP-0ADdL1rk"><img src="imgs/tablet.png" alt=""></a>')
}
function openNav() {
  document.getElementById("navigation").style.width = "100%";
}
function closeNav() {
  document.getElementById("navigation").style.width = "0%";
}
// if ($(window).width() > 479){
// // cache the element
//   var $navBar = $('#navigation');
// // find original navigation bar position
//   var navPos = $navBar.offset().top;
// // on scroll
// $(window).scroll(function() {
// // get scroll position from top of the page
//   var scrollPos = $(this).scrollTop();
// // check if scroll position is >= the nav position
// if (scrollPos > navPos) {
//   $navBar.addClass('fixed');
//   $('#logo-change').attr("src","imgs/logo_color.png");
//   } else {
//     $navBar.removeClass('fixed');
//     $('#logo-change').attr("src","imgs/logo_white.png");
//     }
//   });
// }
